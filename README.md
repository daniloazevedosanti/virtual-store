### Descrição da Web API

Aplicação em Java 11 com Sprig boot / framework, utilizando persistência JPA + PostgresSQL Cloud AmazonAWS.
Patterns: MVC + Java architecture.
- Link heroku: https://webapi2020testings.herokuapp.com/

###### Obs: 
O "application properties" para perfil "test" (spring.profiles.active=test), usa o banco de dados em memória H2.
O "application properties" para perfil "dev" (spring.profiles.active=dev), usa o banco de dados MySQL.
O "application properties" para perfil "prod" (spring.profiles.active=prod), usa o banco de dados 
PostgresSQL Cloud AmazonAWS (especificamente para o Heroku).

###### Docker / Hub container
em atualização....

#### Documentação!
A documentação da API se encontra na pasta raíz do projeto "api-docs.json" ou através da 
execução do projeto em: http://localhost:8080/swagger-ui.html ou https://webapi2020testings.herokuapp.com/swagger-ui.html

#### Teste!
Os testes incluem testagem de requisações para os 3 módulos, para executar, abrir o projeto e executar 
o arquivo "StoreApplicationTests.java", (src>java>test).É possivel também fazer uso da class TestConfig, 
onde há um teste de cenário/simulação de inserção dos produtos, clientes e pedidos, no diretório 
principal "main", em (config>TestConfig.java), para isso mude o application properties para perfil "test" 
(spring.profiles.active=test), e depois veja através de requisições http.

#### Instruções de uso!
De posso do link https://webapi2020testings.herokuapp.com/, 
para acessar os registro utilize uma aplicação para simular requisições http (ex: postman)
existem 3 frente Clientes, Produtos, e pedidos ou compras.

##### TESTE VIA https://webapi2020testings.herokuapp.com/
###### Item A 
####### 1 - Clientes
Para cadastrar um cliente segue o link https://webapi2020testings.herokuapp.com/clients (método POST),
Corpo da requisição: {"name": "NomeDoCliente"} (formato json). 

Para consultar segue o link https://webapi2020testings.herokuapp.com/clients (método GET),

###### 2 - Produtos
Para cadastrar um produtos segue o link https://webapi2020testings.herokuapp.com/products (método POST),
Corpo da requisição: {"name" : "NomeProduto", "price" : 100.00} (formato json). 

Para consultar segue o link https://webapi2020testings.herokuapp.com/clients (método GET),

###### 3 - Pedidos/Compras
Para cadastrar uma compra segue o link https://webapi2020testings.herokuapp.com/orders (método POST),
Corpo da requisição: {"clientId" : IdDoClient, "productId" : IdDoProduto, "quantity": quantidade} (formato json). 

Para consultar segue o link https://webapi2020testings.herokuapp.com/orders (método GET),

#### TESTE LOCAL https://localhost:{8080} "{sua porta de conexão}"
##### Item B -  
Para testes local disponilizo uma classe de teste class, nessa classe se faz um seeding na "mão" ou em um banco local (estou usando o Postgressql), mas 
para tal é preciso modificar o arquivo "application.properties" (local src/main/resources) na linha "spring.profiles.active=prod" mude de 'prod' para:

-'test' --> atraves da classe "TestConfig.java" faz um seeding e gerar um banco em memória, preenchendo as tabelas e daí é possivel ver as informações
via GET com postman ou outro programa de requisição.

-'dev' --> Funciona igual ao Item A, porém é preciso ter o drive/ instância (dependência do maven) banco de dados verificar arquivo 'application.properties'. 
